'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"version.json": "992fa50612fa47706afd3f3e105d67b0",
"index.html": "cbdb1412f0103a520842dc93681363c2",
"/": "cbdb1412f0103a520842dc93681363c2",
"main.dart.js": "4e4a590892c9b218b191bfa7d187598e",
"flutter.js": "7d69e653079438abfbb24b82a655b0a4",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "1b85335ce3ce5bcbbdbea8a34da12574",
"assets/AssetManifest.json": "3807a01e2b73d802c07f01d4da156d3e",
"assets/NOTICES": "016d81f46d7f5a5a99ae66e495edd58f",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/AssetManifest.bin.json": "7a0475f865682ae35dae8c5c6650d643",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "89ed8f4e49bcdfc0b5bfc9b24591e347",
"assets/packages/fluttertoast/assets/toastify.js": "18cfdd77033aa55d215e8a78c090ba89",
"assets/packages/fluttertoast/assets/toastify.css": "910ddaaf9712a0b0392cf7975a3b7fb5",
"assets/packages/stopwordies/assets/jsons/sw-so.json": "d4fa50f22de8065a768f9ecb1754f682",
"assets/packages/stopwordies/assets/jsons/sw-sv.json": "994039ea98a0dd01afd43f026a8bd0ad",
"assets/packages/stopwordies/assets/jsons/sw-th.json": "2cafd5ac0db97e79cdbfed958fcaa911",
"assets/packages/stopwordies/assets/jsons/sw-es.json": "51adb8900e519d98418debe03fa0b2c1",
"assets/packages/stopwordies/assets/jsons/sw-ar.json": "c4f4453445d66fd1b4080989c674a072",
"assets/packages/stopwordies/assets/jsons/sw-id.json": "45144718a16ce58e84b96a30cd80d9e6",
"assets/packages/stopwordies/assets/jsons/sw-no.json": "d2e622bf9f8508b7ce537e689ce4d91a",
"assets/packages/stopwordies/assets/jsons/sw-lv.json": "39623a81c51e9a2e095ba5d31c24c1a4",
"assets/packages/stopwordies/assets/jsons/sw-da.json": "3addded6e6e0dcfec0b9d3f9bd7a259d",
"assets/packages/stopwordies/assets/jsons/sw-sw.json": "993485eb5fd124a679b68aa015cb1fb2",
"assets/packages/stopwordies/assets/jsons/sw-af.json": "48dee102abdab31b9ba0ea41a3d17e28",
"assets/packages/stopwordies/assets/jsons/sw-fa.json": "5814bc930aa7b654e059060492260a46",
"assets/packages/stopwordies/assets/jsons/sw-vi.json": "e9376778578d7dcd0144c036b7c9efe7",
"assets/packages/stopwordies/assets/jsons/sw-cs.json": "a465a4a3d5b1338a283a1073a5f6dac9",
"assets/packages/stopwordies/assets/jsons/sw-ko.json": "32e036d9c65e0cc3c824e97e9e7ae5ff",
"assets/packages/stopwordies/assets/jsons/sw-gl.json": "ed5fd4d126b2b11fb94200ba1c074f6a",
"assets/packages/stopwordies/assets/jsons/sw-eu.json": "c0a0f2b730725a3ac5b0681d8ecf26c6",
"assets/packages/stopwordies/assets/jsons/sw-he.json": "dce5b75e6a40bdd54a4a34d936417413",
"assets/packages/stopwordies/assets/jsons/sw-el.json": "4f1ad9caead21bf85a5378d477b523be",
"assets/packages/stopwordies/assets/jsons/sw-gu.json": "ecdaaa32669e44839eb8d78f36e5f7f4",
"assets/packages/stopwordies/assets/jsons/sw-br.json": "e2f1a140823ef79cf903f3da01039c73",
"assets/packages/stopwordies/assets/jsons/sw-ro.json": "a75a2641c0c3dcfdb77e01ea9908e414",
"assets/packages/stopwordies/assets/jsons/sw-bg.json": "6d454df1165def2f039a7bdea31232da",
"assets/packages/stopwordies/assets/jsons/sw-ca.json": "d5790c2050e8760d648b486406b2e38a",
"assets/packages/stopwordies/assets/jsons/sw-hi.json": "deedbfc3e851e4d815deea3e3b142fa4",
"assets/packages/stopwordies/assets/jsons/sw-ga.json": "be770187e0b94613f76d1eacc230837b",
"assets/packages/stopwordies/assets/jsons/sw-fr.json": "c47f0ad12d194b01695c1d09139f02a6",
"assets/packages/stopwordies/assets/jsons/sw-zu.json": "fbf98d63c2b315a2cd3d71f8441e807a",
"assets/packages/stopwordies/assets/jsons/sw-et.json": "7b2131e5986a160b36eb52fea3b4995d",
"assets/packages/stopwordies/assets/jsons/sw-it.json": "f2d4bc0fc185a46c819b06dec838e2c1",
"assets/packages/stopwordies/assets/jsons/sw-tl.json": "13b2cda8740c0684b0f6b545230966a2",
"assets/packages/stopwordies/assets/jsons/sw-hr.json": "8a33653a32949da457df1a39e2f07baf",
"assets/packages/stopwordies/assets/jsons/sw-sk.json": "d919a91282435c2cb379f576d08a1165",
"assets/packages/stopwordies/assets/jsons/sw-pt.json": "9b7235fb9b075d404d3ef5ff60a39123",
"assets/packages/stopwordies/assets/jsons/sw-en.json": "d29af31cdb15b56c4baf812cae27675c",
"assets/packages/stopwordies/assets/jsons/sw-eo.json": "b822dd283ab3b4547f87862ff49d4c6f",
"assets/packages/stopwordies/assets/jsons/sw-ur.json": "0c13fdf6439dacc95622e0c8fd2258d4",
"assets/packages/stopwordies/assets/jsons/sw-ku.json": "d290960692df3c159dcc7a407bf86dfb",
"assets/packages/stopwordies/assets/jsons/sw-de.json": "90fcd54bc311a23da39938c0335e9438",
"assets/packages/stopwordies/assets/jsons/sw-uk.json": "da6f4a19bb41d7fa532f5c2e26ad3f6c",
"assets/packages/stopwordies/assets/jsons/sw-pl.json": "89230b7050892093fc50cc2ed8f188d1",
"assets/packages/stopwordies/assets/jsons/sw-ru.json": "17395ef3d5e41a8340efdadcc3e3d593",
"assets/packages/stopwordies/assets/jsons/sw-fi.json": "4c0350c18b1ba76bc40a0e05157bb192",
"assets/packages/stopwordies/assets/jsons/sw-ja.json": "a4fa6ea7704056d851ab335f05750654",
"assets/packages/stopwordies/assets/jsons/sw-ha.json": "6f0740041e532c3ee4d9253d10ee39aa",
"assets/packages/stopwordies/assets/jsons/sw-yo.json": "0b953c3267df7f87e189a45223424940",
"assets/packages/stopwordies/assets/jsons/sw-nl.json": "cd64a6404a645108db7bef5ef2352198",
"assets/packages/stopwordies/assets/jsons/sw-st.json": "4de21db6f32adb2f2fc28e379418c49f",
"assets/packages/stopwordies/assets/jsons/sw-ms.json": "8608a3b79db205b3bfd75a64235eb83c",
"assets/packages/stopwordies/assets/jsons/sw-mr.json": "02b96cc56926cc9d7fd8a4ed2273fd94",
"assets/packages/stopwordies/assets/jsons/sw-hu.json": "5db18c259d8722b0700a8d7e6f644804",
"assets/packages/stopwordies/assets/jsons/sw-lt.json": "9fa3da57efd572e5bdedc70282a7d023",
"assets/packages/stopwordies/assets/jsons/sw-la.json": "e159b8fd394a112e2cf88eec4bab7241",
"assets/packages/stopwordies/assets/jsons/sw-bn.json": "657a0e19dcddb36c4d0b2772a1d9c4a9",
"assets/packages/stopwordies/assets/jsons/sw-zh.json": "0d52dc4038fadf2adfd8d88627f8c62b",
"assets/packages/stopwordies/assets/jsons/sw-hy.json": "c040596e0ec474195b32e51561aaee21",
"assets/packages/stopwordies/assets/jsons/sw-sl.json": "6ee77374fc2a2dfad44a610b0b0e384f",
"assets/packages/stopwordies/assets/jsons/sw-tr.json": "442c708eae931f752b749a74fb3228f4",
"assets/shaders/ink_sparkle.frag": "4096b5150bac93c41cbc9b45276bd90f",
"assets/AssetManifest.bin": "800a651731fcfc39ce543c80c6e8eee5",
"assets/fonts/MaterialIcons-Regular.otf": "4a605f5da94f25c87a21ab9601e256e1",
"assets/assets/gyapu.ico": "ec3c655d5128f738bbd9421fe3808bd3",
"assets/assets/hamrobazaar.png": "daf171879f9b4899ebfcd069b40ad150",
"canvaskit/skwasm.js": "87063acf45c5e1ab9565dcf06b0c18b8",
"canvaskit/skwasm.wasm": "2fc47c0a0c3c7af8542b601634fe9674",
"canvaskit/chromium/canvaskit.js": "0ae8bbcc58155679458a0f7a00f66873",
"canvaskit/chromium/canvaskit.wasm": "143af6ff368f9cd21c863bfa4274c406",
"canvaskit/canvaskit.js": "eb8797020acdbdf96a12fb0405582c1b",
"canvaskit/canvaskit.wasm": "73584c1a3367e3eaf757647a8f5c5989",
"canvaskit/skwasm.worker.js": "bfb704a6c714a75da9ef320991e88b03"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
