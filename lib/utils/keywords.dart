import "package:stopwordies/stopwordies.dart";

Future<List<String>> generateKeywords(String name, {int maxLength = 3}) async {
  // Remove punctuation and lowercase the product name.
  var productName = name.toLowerCase();
  productName = productName.replaceAll(RegExp(r'[^\w\s]'), '');

  // Split the product name into individual words.
  var words = productName.split(' ');

  // Removing double spaces
  words = words.where((w) => w.trim().length > 1).toList();

  // Remove the English stop words.
  final stopWords = await StopWordies.getFor(locale: SWLocale.en);
  words = _removeStopwords(words, [...stopWords, 'upto', 'onto']);

  // Generate all possible combinations of words, limiting the keywords to a maximum of 3 words.
  final keywords = <String>[];
  for (int i = 0; i < words.length; i++) {
    for (int j = i + 1; j <= words.length; j++) {
      if (j - i <= maxLength && j - i > 1) {
        final k = words.sublist(i, j).join(' ').trim();
        keywords.add(k);
      }
    }
  }

  // Return the list of keywords.
  return keywords;
}

List<String> _removeStopwords(List<String> words, List<String> list) {
  final finalWords = <String>[];
  for (var w in words) {
    if (!list.contains(w)) {
      finalWords.add(w);
    }
  }
  return finalWords;
}
