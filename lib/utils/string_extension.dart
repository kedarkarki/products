import 'package:intl/intl.dart';

extension StringUtilEx on String {
  num get toNum => num.tryParse(this) ?? 0;
  String get priceFormatted =>
      'Rs. ${NumberFormat('##,##,###.##').format(toNum)}';
}
