import 'package:flutter/material.dart';
import 'package:products/ui/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerItem extends StatelessWidget {
  const ShimmerItem({
    required this.height,
    required this.borderRadius,
    this.width,
    super.key,
  });
  final double height;
  final double? width;
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: AppColors.accent,
      highlightColor: AppColors.primary,
      child: SizedBox(
        width: width ?? double.infinity,
        height: height,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(
              borderRadius,
            ),
          ),
        ),
      ),
    );
  }
}

class ShimmerScroll extends StatelessWidget {
  const ShimmerScroll({
    required this.height,
    required this.width,
    required this.borderRadius,
    this.padding = const EdgeInsets.symmetric(vertical: 16),
    this.childGap = 32,
    super.key,
  });
  final double height;
  final double width;
  final double borderRadius;
  final EdgeInsetsGeometry padding;
  final double childGap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            ShimmerItem(
              height: height,
              borderRadius: borderRadius,
              width: width,
            ),
            SizedBox(
              width: childGap,
            ),
            ShimmerItem(
              height: height,
              borderRadius: borderRadius,
              width: width,
            ),
            SizedBox(
              width: childGap,
            ),
            ShimmerItem(
              height: height,
              borderRadius: borderRadius,
              width: width,
            ),
          ],
        ),
      ),
    );
  }
}
