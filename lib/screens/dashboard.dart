import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/controllers/home.dart';
import 'package:products/widgets/product_tile.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final homeController = Get.find<HomeController>();
  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: SearchBar(
              controller: searchController,
              hintText: 'Search product here...',
              shape: const MaterialStatePropertyAll(RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8)),
              )),
              leading: const Icon(
                Icons.search,
                color: Colors.blueGrey,
              ),
              onSubmitted: (s) {
                if (s.isEmpty) {
                  return;
                }
                homeController.loadProducts(keyword: s);
              },
            ),
          ),
          Expanded(
            child: homeController.obx(
              onLoading: const Center(
                child: CircularProgressIndicator(),
              ),
              onEmpty: const Center(
                child: Text('No products available'),
              ),
              onError: (e) => Center(
                child: Text(e ?? 'Something went wrong'),
              ),
              (state) {
                if (state == null) {
                  return const SizedBox.shrink();
                }
                return RefreshIndicator(
                  onRefresh: () {
                    searchController.clear();
                    return homeController.refreshProducts();
                  },
                  child: GridView.builder(
                    itemCount: state.length,
                    itemBuilder: (context, index) => ProductTile(
                      product: state[index],
                    ),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
