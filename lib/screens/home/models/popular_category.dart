enum PopularCategory {
  electronics('electronics'),
  clothing('clothing'),
  books('books'),
  gaming('gaming'),
  groceries('groceries');

  final String slug;

  const PopularCategory(this.slug);
}
