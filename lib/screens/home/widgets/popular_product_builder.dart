import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/model/product.dart';
import 'package:products/screens/product_detail.dart';
import 'package:products/ui/colors.dart';
import 'package:products/utils/string_extension.dart';
import 'package:products/widgets/shimmer.dart';

class PopularProductBuilder extends StatelessWidget {
  const PopularProductBuilder({super.key, required this.future});
  final Future<List<Product>> future;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Product>>(
      future: future,
      builder: (ctx, snapshot) {
        if (!snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ListView.separated(
              itemBuilder: (c, i) {
                return const ShimmerItem(height: 96, borderRadius: 16);
              },
              separatorBuilder: (c, i) => const SizedBox(
                height: 8,
              ),
              itemCount: 20,
            ),
          );
        }
        final products = snapshot.data!;
        return ListView.separated(
          itemBuilder: (c, i) {
            final product = products[i];
            return ListTile(
              onTap: () => Get.to(() => ProductDetailPage(product: product)),
              leading: CachedNetworkImage(
                imageUrl: product.imageUrl,
                height: 50,
                width: 50,
              ),
              title: Text(product.name),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        product.price.priceFormatted,
                        style: context.textTheme.bodyLarge?.apply(
                          color: AppColors.primary,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        product.originalPrice.priceFormatted,
                        style: context.textTheme.bodyMedium?.copyWith(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                    ],
                  ),
                  DecoratedBox(
                    decoration: BoxDecoration(
                        color: Colors.green.shade50,
                        borderRadius: BorderRadius.circular(8)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        product.discountPercent,
                        style: context.textTheme.bodyMedium?.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
          separatorBuilder: (c, i) => const SizedBox(
            height: 8,
          ),
          itemCount: products.length,
        );
      },
    );
  }
}
