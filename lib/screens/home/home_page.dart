import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/controllers/home.dart';
import 'package:products/screens/home/controllers/popular_products.dart';
import 'package:products/screens/home/models/popular_category.dart';
import 'package:products/screens/home/widgets/featured_product_card.dart';
import 'package:products/screens/home/widgets/popular_product_builder.dart';
import 'package:products/screens/search/search_page.dart';
import 'package:products/widgets/shimmer.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();
    return Scaffold(
      body: GetBuilder<PopularProductsController>(
          init: PopularProductsController(),
          builder: (controller) {
            return DefaultTabController(
              length: PopularCategory.values.length,
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Stack(
                      children: [
                        ClipPath(
                          clipper: _BezierClipper(),
                          child: Container(
                            color: context.theme.colorScheme.primary,
                            height: 450,
                            width: double.infinity,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 46,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  'SUGGESTED FOR YOU',
                                  style: context.textTheme.bodyLarge?.apply(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(
                                  'Find the products you love.',
                                  style: context.textTheme.titleMedium?.apply(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              homeController.obx(
                                  onLoading: const ShimmerScroll(
                                    height: 300,
                                    width: 300,
                                    borderRadius: 16,
                                    padding: EdgeInsets.all(16),
                                  ), (state) {
                                if (state == null) {
                                  return const SizedBox.shrink();
                                }

                                return SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      for (int i = 0; i < state.length; i++)
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: FeaturedProductCard(
                                            product: state[i],
                                          ),
                                        ),
                                    ],
                                  ),
                                );
                              })
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SliverFillRemaining(
                    fillOverscroll: true,
                    hasScrollBody: true,
                    child: SafeArea(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Most Popular Items By Category',
                              style: context.textTheme.titleMedium,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            TabBar(
                              isScrollable: true,
                              tabAlignment: TabAlignment.start,
                              tabs: List.generate(
                                PopularCategory.values.length,
                                (index) => Tab(
                                  text: PopularCategory
                                      .values[index].slug.capitalize,
                                ),
                                growable: false,
                              ),
                            ),
                            Expanded(
                              child: TabBarView(
                                children: [
                                  for (final category in PopularCategory.values)
                                    PopularProductBuilder(
                                      future: controller.getProducts(category),
                                    ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Get.to(() => const SearchPage()),
        child: const Icon(Icons.search),
      ),
    );
  }
}

/// Custom Path Clipper for Rounded Container in Drawer Menu
class _BezierClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0, size.height * 0.4);
    path.quadraticBezierTo(
      size.width / 2,
      size.height,
      size.width,
      size.height,
    );
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
