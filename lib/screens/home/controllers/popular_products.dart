import 'package:get/get.dart';
import 'package:products/model/product.dart';
import 'package:products/scrapers/sources/daraz.dart';
import 'package:products/screens/home/models/popular_category.dart';

class PopularProductsController extends GetxController {
  final tabProducts = <PopularCategory, List<Product>>{};

  Future<List<Product>> getProducts(PopularCategory category) async {
    try {
      if (tabProducts.containsKey(category)) {
        return tabProducts[category]!;
      }
      final products = await DarazClient.instance.getProducts(category.slug);
      tabProducts.putIfAbsent(category, () => [...products]);
      return products;
    } catch (_) {
      return [];
    }
  }
}
