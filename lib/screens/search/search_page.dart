import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/screens/home/widgets/popular_product_builder.dart';
import 'package:products/screens/search/controller/search_controller.dart';
import 'package:products/widgets/shimmer.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: GetBuilder<SearchProductsController>(
            init: SearchProductsController(),
            builder: (controller) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: SearchBar(
                      focusNode: controller.focusNode,
                      controller: controller.searchTextController,
                      hintText: 'Search product here...',
                      hintStyle: MaterialStatePropertyAll(
                        context.textTheme.bodyMedium,
                      ),
                      textStyle: MaterialStatePropertyAll(
                        context.textTheme.bodyMedium,
                      ),
                      shape:
                          const MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      )),
                      leading: const Icon(
                        Icons.search,
                        color: Colors.blueGrey,
                      ),
                      onSubmitted: (s) {
                        if (s.isEmpty) {
                          return;
                        }
                        controller.searchProduct(s);
                      },
                    ),
                  ),
                  Expanded(
                    child: controller.obx(
                      onLoading: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: ListView.separated(
                          itemBuilder: (c, i) {
                            return const ShimmerItem(
                              height: 96,
                              borderRadius: 16,
                            );
                          },
                          separatorBuilder: (c, i) => const SizedBox(
                            height: 8,
                          ),
                          itemCount: 20,
                        ),
                      ),
                      onError: (err) {
                        return Center(
                          child: Text(err ?? 'Something went wrong'),
                        );
                      },
                      (state) {
                        if (state == null) {
                          return const SizedBox.shrink();
                        }
                        return PopularProductBuilder(
                          future: (() async => state)(),
                        );
                      },
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
