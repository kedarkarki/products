import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/model/product.dart';
import 'package:products/scrapers/sources/daraz.dart';

class SearchProductsController extends GetxController
    with StateMixin<List<Product>> {
  late final TextEditingController searchTextController;
  late final FocusNode focusNode;

  @override
  void onInit() {
    searchTextController = TextEditingController();
    focusNode = FocusNode();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    change(null, status: RxStatus.error(''));
    if (focusNode.canRequestFocus) {
      focusNode.requestFocus();
    }
  }

  @override
  void onClose() {
    searchTextController.dispose();
    focusNode.dispose();
    super.onClose();
  }

  Future<void> searchProduct(String keyword) async {
    try {
      change(null, status: RxStatus.loading());
      final results = await DarazClient.instance.getProducts(keyword);
      if (results.isEmpty) {
        change(null, status: RxStatus.empty());
      } else {
        change([...results], status: RxStatus.success());
      }
    } catch (_) {
      change(null, status: RxStatus.empty());
    }
  }
}
