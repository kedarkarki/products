import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/controllers/alternatives.dart';
import 'package:products/model/product.dart';
import 'package:products/ui/colors.dart';
import 'package:products/utils/string_extension.dart';
import 'package:url_launcher/url_launcher.dart';

class AlternativesScreen extends StatefulWidget {
  const AlternativesScreen({required this.product, super.key});
  final Product product;

  @override
  State<AlternativesScreen> createState() => _AlternativesScreenState();
}

class _AlternativesScreenState extends State<AlternativesScreen> {
  late final AlternativesController alterNativeController;

  @override
  void initState() {
    alterNativeController =
        Get.put(AlternativesController.getInstance(widget.product));
    alterNativeController.loadAlternatives();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Alternatives'),
      ),
      body: alterNativeController.obx(
        onLoading: const Center(
          child: CircularProgressIndicator(),
        ),
        onEmpty: const Center(
          child: Text('No alternatives available'),
        ),
        onError: (e) => Center(
          child: Text(e ?? 'Something went wrong'),
        ),
        (state) {
          if (state == null) {
            return const Center(
              child: Text('Something went wrong'),
            );
          }
          return ListView.builder(
            itemBuilder: (ctx, i) {
              final product = state[i];
              return ListTile(
                onTap: () async {
                  final uri = Uri.parse(product.productUrl);

                  await launchUrl(uri);
                },
                isThreeLine: true,
                leading: CachedNetworkImage(
                  imageUrl: product.imageUrl,
                  height: 60,
                  width: 60,
                ),
                title: Text(
                  product.name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                trailing: Image.asset(
                  product.source == Source.gyapu
                      ? 'assets/gyapu.ico'
                      : 'assets/hamrobazaar.png',
                  width: 32,
                  height: 32,
                ),
                subtitle: Text(
                  product.price.priceFormatted,
                  style: context.textTheme.bodyLarge?.apply(
                    color: AppColors.primary,
                  ),
                ),
              );
            },
            itemCount: state.length,
          );
        },
      ),
    );
  }
}
