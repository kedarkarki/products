import 'dart:convert';

import 'package:html/parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:products/model/product.dart';

class DarazClient extends http.BaseClient {
  final http.Client client;

  DarazClient._(this.client);

  static final _instance = DarazClient._(http.Client());

  static DarazClient get instance => _instance;

  static const baseUrl = 'www.daraz.com.np';
  static const searchPath = 'catalog';

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return client.send(request);
  }

  Future<List<Product>> getProducts(String keyword) async {
    final uri = Uri.https(baseUrl, searchPath, {'q': keyword});
    final response = await get(uri);
    if (response.statusCode >= 300) {
      throw 'Invalid Response';
    }
    final doc = parser.parse(response.body);
    final head = doc.getElementsByTagName('head').first;
    final script = head
        .getElementsByTagName('script')[3]
        .text
        .replaceFirst('window.pageData=', '');
    final parsedData = json.decode(script) as Map<String, dynamic>;
    final listItems = parsedData['mods']['listItems'] as List;
    return listItems.map((item) {
      final desc = (item['description'] as List?)?.map((e) => '$e').toList();

      return Product(
          name: item['name'] ?? '',
          productUrl: item['productUrl'] ?? '',
          imageUrl: item['image'] ?? '',
          price: item['price'] ?? '',
          description: desc ?? [],
          originalPrice: item['originalPrice'] ?? item['price'] ?? '',
          source: Source.daraz);
    }).toList();
  }
}
