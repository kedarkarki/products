import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:products/model/product.dart';

class HamrobazaarClient extends http.BaseClient {
  final http.Client client;

  HamrobazaarClient._(this.client);

  static final _instance = HamrobazaarClient._(http.Client());

  static HamrobazaarClient get instance => _instance;

  static const baseUrl = 'api.hamrobazaar.com';
  static const searchPath = 'api/Search/Products';

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers
        .putIfAbsent('apikey', () => '09BECB8F84BCB7A1796AB12B98C1FB9E');
    return client.send(request);
  }

  Future<List<Product>> getProducts(String keyword, [int limit = 10]) async {
    final uri = Uri.https(baseUrl, searchPath);
    final response = await post(uri,
        body: json.encode({
          "filterParams": {
            "category": "",
            "brand": "",
            "condition": 1,
            "isPriceNegotiable": null,
            "priceFrom": 0,
            "priceTo": 0,
            "categoryIds": "",
            "brandIds": "",
            "advanceFilter": ""
          },
          "pageNumber": 1,
          "pageSize": limit,
          "searchParams": {
            "searchBy": "",
            "searchValue": keyword.toLowerCase(),
            "Latitude": 0,
            "Longitude": 0,
            "searchByDistance": 0
          },
          "sortParam": 0,
          "Latitude": 0,
          "Longitude": 0,
          "deviceId": "e5e7cc9ed2b3b76c",
          "deviceSource": "Android",
          "isHBSelect": false,
          "tempCategory": "",
          "tempSubCategory": "",
          "attributeFilter": [],
          "locationDescription": ""
        }));
    if (response.statusCode >= 300) {
      throw 'Invalid Response';
    }
    final parsedResp = json.decode(response.body) as Map<String, dynamic>;
    final listItems = parsedResp['data'] as List;

    return listItems.map((item) {
      final c = item['categoryName']?.replaceAll("'", '') ?? '';
      final category = RegExp(r'[a-zA-Z]+')
          .allMatches(c)
          .map((e) => e[0]?.toLowerCase())
          .join('-');
      final name = RegExp(r'[a-zA-Z]+')
          .allMatches('${item['name'] ?? ''}')
          .map((e) => e[0]?.toLowerCase())
          .join('-');
      final location =
          item['location']['locationDescription'].split(' ').last.toLowerCase();
      final url =
          'https://hamrobazaar.com/$category/$name-in-$location/${item['id'].replaceAll('-', '').toLowerCase()}';

      return Product(
          name: item['name'] ?? '',
          productUrl: url,
          imageUrl: item['imageUrl'],
          price: item['price']?.toString() ?? '',
          description: [item['description'] ?? ''],
          source: Source.hamrobazaar);
    }).toList();
  }
}
