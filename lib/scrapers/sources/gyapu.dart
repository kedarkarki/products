import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:products/model/product.dart';

class GyapuClient extends http.BaseClient {
  final http.Client client;

  GyapuClient._(this.client);

  static final _instance = GyapuClient._(http.Client());

  static GyapuClient get instance => _instance;

  static const baseUrl = 'www.gyapu.com';
  static const searchPath = 'api/elastic/search';

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return client.send(request);
  }

  Future<List<Product>> getProducts(String keyword, [int limit = 10]) async {
    final uri = Uri.https(baseUrl, searchPath, {
      'q': keyword,
      'size': '$limit',
      'page': '1',
    });
    final response = await get(uri);
    if (response.statusCode >= 300) {
      throw 'Invalid Response';
    }
    final parsedResp = json.decode(response.body) as Map<String, dynamic>;
    final listItems = parsedResp['data']['products'] as List;

    return listItems.map((item) {
      final desc =
          (item['meta_tags'] as List?)?.map((e) => '$e').take(5).toList();
      final pUrl = 'https://www.gyapu.com/detail/${item['url_key']}';
      final imgUrl =
          'https://gyapu.com/${item['image'][0]['document']['path'].replaceAll(' ', '%20')}';

      return Product(
          name: item['name'] ?? '',
          productUrl: pUrl,
          imageUrl: imgUrl,
          price: item['min_sales_price']?.toString() ?? '',
          description: desc ?? [],
          source: Source.gyapu);
    }).toList();
  }
}
