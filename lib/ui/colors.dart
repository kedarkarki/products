import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xfff3a021);
  static const secondary = Color(0xff5fa1d5);
  static const accent = Color(0xffb1ccf2);
}
