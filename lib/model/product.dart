import 'package:products/utils/string_extension.dart';

class Product {
  final String name;
  final String productUrl;
  final String imageUrl;
  final String price;
  final List<String> description;
  final String originalPrice;
  final Source source;

  const Product(
      {required this.name,
      required this.productUrl,
      required this.imageUrl,
      required this.price,
      required this.description,
      this.originalPrice = '0',
      required this.source});

  String get discountPercent {
    final discount =
        ((originalPrice.toNum - price.toNum) / originalPrice.toNum) * 100;
    return '${discount.floor()}% OFF';
  }

  Map<String, dynamic> toMap() => {
        'name': name,
        'productUrl': productUrl,
        'imageUrl': imageUrl,
        'price': price,
        'description': description,
        'source': source.name
      };
}

enum Source { daraz, gyapu, sastodeal, hamrobazaar }
