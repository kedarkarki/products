import 'package:get/get.dart';
import 'package:products/model/product.dart';
import 'package:products/scrapers/sources/gyapu.dart';
import 'package:products/scrapers/sources/hamrobazaar.dart';
import 'package:products/utils/keywords.dart';

class AlternativesController extends GetxController
    with StateMixin<List<Product>> {
  final GyapuClient gyapuClient;
  final HamrobazaarClient hamrobazaarClient;
  final Product actualProduct;

  AlternativesController(
      this.gyapuClient, this.hamrobazaarClient, this.actualProduct);

  factory AlternativesController.getInstance(Product actualProduct) =>
      AlternativesController(
          GyapuClient.instance, HamrobazaarClient.instance, actualProduct);

  Future<void> loadAlternatives() async {
    try {
      final allKeywords = await generateKeywords(actualProduct.name);
      final keywords = [allKeywords[1], allKeywords[0], allKeywords[2]];
      final gyapuProducts = <Product>[];
      final hamrobazarProducts = <Product>[];
      for (var keyword in keywords) {
        final gp = await gyapuClient.getProducts(keyword);
        if (gp.isNotEmpty) {
          gyapuProducts.assignAll(gp);
          break;
        }
      }
      for (var keyword in keywords) {
        final gp = await hamrobazaarClient.getProducts(keyword);
        if (gp.isNotEmpty) {
          hamrobazarProducts.assignAll(gp);
          break;
        }
      }
      final finalProducts = [...gyapuProducts, ...hamrobazarProducts];
      finalProducts.shuffle();
      if (finalProducts.isEmpty) {
        change(null, status: RxStatus.empty());
      } else {
        change(finalProducts, status: RxStatus.success());
      }
    } catch (e) {
      change(null, status: RxStatus.error('Could not load alternatives'));
    }
  }
}
