import 'dart:math';

import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:products/model/product.dart';
import 'package:products/scrapers/sources/daraz.dart';

class HomeController extends GetxController with StateMixin<List<Product>> {
  final DarazClient darazClient;

  HomeController(this.darazClient);

  factory HomeController.getInstance() => HomeController(DarazClient.instance);

  @override
  void onInit() {
    loadProducts(keyword: 'smart');
    super.onInit();
  }

  Future<void> loadProducts({String? keyword}) async {
    change(null, status: RxStatus.loading());
    try {
      final products =
          await darazClient.getProducts(keyword ?? _getRandomChar());
      if (products.isEmpty) {
        change(null, status: RxStatus.empty());
      } else {
        change([...products.take(8)], status: RxStatus.success());
      }
    } catch (_) {
      change(null,
          status: RxStatus.error('Could not load products from daraz'));
    }
  }

  String _getRandomChar() {
    const chars = 'wertuiopasdfghjklcvbnm';
    final charIndex = Random().nextInt(chars.length);
    return chars[charIndex];
  }

  Future<void> refreshProducts() async {
    await loadProducts();
  }
}
