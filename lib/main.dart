import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:products/controllers/home.dart';
import 'package:products/screens/home/home_page.dart';
import 'package:products/ui/colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Products',
      onInit: () {
        Get.put(HomeController.getInstance());
      },
      theme: ThemeData(
        colorScheme: const ColorScheme.light(
          primary: AppColors.primary,
        ),
        useMaterial3: true,
        appBarTheme: const AppBarTheme(
            centerTitle: true,
            titleTextStyle: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
              fontSize: 24,
            )),
        textTheme: const TextTheme(
          bodyMedium: TextStyle(
            fontSize: 14,
          ),
          bodyLarge: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
          ),
          titleMedium: TextStyle(
            fontSize: 18,
          ),
        ),
      ),
      home: const HomePage(),
    );
  }
}
